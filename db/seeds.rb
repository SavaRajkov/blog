# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
#
# Primer za programsko ubacivanje podataka i njihovo povezivanje.
# Sve što važi za seeds.rb važi za svaki pojedinačni 'db:migrate'.rb.
# Podsetnik: svi definisani 'db:migrate'.rb se nalaze u folderu db/migrate.
# Bitno je novo dodati (ili pronadjeni) slog smestiti u objektnu promenjivu.
# Pri dodavanju dece (za taj slog) u druge tabele mogu se koristiti atributi objektne promenjive.
#
# U ovom primeru ćemo kreirati nekoliko blog postova i po nekoliko komentara za svaki od njih.

# 2014-04-dd Verzija sa Post.create.
# 2015-12-15 Verzija sa Post.first_or_create i post_records = [ {}, {} ].

# Definisanje i dodavanje u bazu zapisa za master tabelu Post.
# Mada se svi Post zapisi mogu ubaciti zajedno, 
# bolje je razdvojiti ih pojedinačno, jer će se tako dobiti i id svakog pojedinačnog zapisa, 
# i to se može odmah upotrebiti za ubacivanje svih povezanih komentara u tabelu Comment.

# post_records = [
#   { title: '1st čćšđž seeded post', body: 'body for 1st ČĆŠĐŽ seeded post' }, 
#   { title: '2nd ČĆŠĐŽ seeded post', body: 'body for 2nd ČĆŠĐŽ seeded post' }
# ]
# post_records.each do |record|
#   Post.where(record).first_or_create
# end

posts = Post.where({ title: '1st from seeds.rb ČĆŽŠĐčćžšđ', body: '1st from seeds.rb ČĆŽŠĐčćžšđ' }).first_or_create
comment_records = [
  { post_id: posts.id, body: '1st comment for 1st post (with programmatically setup of id) ČĆŽŠĐčćžšđ' },
  { post_id: posts.id, body: '2nd comment for 1st post (with programmatically setup of id) ČĆŽŠĐčćžšđ' },
]
comment_records.each do | record |
  Comment.where(record).first_or_create
end
posts = Post.where({ title: '2nd from seeds.rb ČĆŽŠĐčćžšđ', body: '2nd from seeds.rb ČĆŽŠĐčćžšđ' }).first_or_create
comment_records = [
  { post_id: posts.id, body: '1st comment for 2nd post (with programmatically setup of id) ČĆŽŠĐčćžšđ' },
  { post_id: posts.id, body: '2nd comment for 2nd post (with programmatically setup of id) ČĆŽŠĐčćžšđ' },
]
comment_records.each do | record |
  Comment.where(record).first_or_create
end

